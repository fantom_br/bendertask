#ifndef StrReverse_h
#define StrReverse_h

#import <Foundation/Foundation.h>

@interface StrReverse: NSObject

- (id)initWithString:(NSString*)string;
- (void)stringReverse:(NSString*)string;

@end

#endif // StrReverse_h