#ifndef Pyramid2_h
#define Pyramid2_h

#import <Foundation/Foundation.h>

@interface Pyramid2: NSObject

- (id)initWithPyramidSize:(int)size;
- (void)printPyramid:(int)size;

@end

#endif //Pyramid2_h