

#import "strReverce.h"

@implementation StrReverse

- (id)initWithString:(NSString*)string
{
    self = [super init];
    if (self) {
        [self stringReverse:string];
    }
    return self;
}

- (void)stringReverse:(NSString*)string
{
    NSMutableString* reversedString = [NSMutableString new];
    int charIndex = (int)[string length];

    for (; charIndex > 0; charIndex--) {
        NSRange subStrRange = NSMakeRange(charIndex-1, 1);
        [reversedString appendString:[string substringWithRange:subStrRange]];
    }

    NSLog(@"%@", reversedString);

}

@end

