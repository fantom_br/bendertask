#import "nextMultiple.h"

@implementation NextMultiple

- (id)initWithValues:(int)dividend secondValue:(int)divisor
{
    self = [super init];
    if (self) {
        [self calculate:dividend secondValue:divisor];
    }
    return self;
}

- (void)calculate:(int)dividend secondValue:(int)divisor
{
    int counter = dividend % divisor;
    int multiple;

    if ( counter <= 0 ) {
        multiple = dividend - counter;
    } else if ( divisor < 0 ) {
        multiple = dividend - counter - divisor;
    } else {
        multiple = dividend - counter + divisor;
    }
    NSLog(@"Next multiple of %d and %d is %d", dividend, divisor, multiple);
}

@end