#ifndef Square2_h
#define Square2_h

#import <Foundation/Foundation.h>

@interface Square2: NSObject

- (id)initWithSquareSize:(int)size;
- (void)printSquare:(int)size;

@end

#endif // Square2_h