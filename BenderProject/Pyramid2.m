
#import "Pyramid2.h"

@implementation Pyramid2

- (id)initWithPyramidSize:(int)size
{
    self = [super init];
    if (self) {
        [self printPyramid:size];
    }
    return self;
}

- (void)printPyramid:(int)size
{
    NSMutableString* cachedValues = [NSMutableString new];
    int counter = 1;

    for (int row = 1; row < size; row++) {
        for (int col = 1; col < row; col++) {
            [cachedValues appendFormat:@"%d ", counter];
            counter += 1;
        }
        [cachedValues appendFormat:@"%d\n", counter];
        counter += 1;
    }

    for ( int col = 1; col < size; col++ ) {
            [cachedValues appendFormat:@"%d ", counter];
            counter += 1;
    }
    [cachedValues appendFormat:@"%d", counter];
    
    NSLog(@"%@", cachedValues);
}
@end