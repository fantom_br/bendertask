
#ifndef Next_Multiple_h
#define Next_Multiple_h

#import <Foundation/Foundation.h>

@interface NextMultiple: NSObject

- (id)initWithValues:(int)dividend secondValue:(int)divisor;
- (void)calculate:(int)dividend secondValue:(int)divisor;

@end

#endif // Next_Multiple_h