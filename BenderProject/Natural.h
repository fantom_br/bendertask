//
//  Natural.h
//  Home task
//
//  Created by Fantom on 1/26/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#ifndef Home_task_Natural_h
#define Home_task_Natural_h

@interface Natural : NSObject

- (id)initWithNaturalLimit:(int)limit;

@end

#endif
