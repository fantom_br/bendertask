//
//  main.m
//  BenderProject
//
//  Created by Fantom on 1/31/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "nextMultiple.h"
#import "Natural.h"
#import "pyramid2.h"
#import "square2.h"
#import "strReverce.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NextMultiple* nextMultiple = [[NextMultiple alloc] initWithValues:10 secondValue:3];
        Natural* natural = [[Natural alloc] initWithNaturalLimit:10];
        Pyramid2* pyramid = [[Pyramid2 alloc] initWithPyramidSize:5];
        Square2* square = [[Square2 alloc] initWithSquareSize:5];
        StrReverse* strReverse = [[StrReverse alloc] initWithString:@"Hello, World!"];
    }
    
    return 0;
}

