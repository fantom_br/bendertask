
#import "Square2.h"

@implementation Square2

- (id)initWithSquareSize:(int)size
{
    self = [super init];
    if (self) {
        [self printSquare:size];
    }
    return self;
}

- (void)printSquare:(int)size
{
    NSMutableString *cachedValues = [NSMutableString new];
    int counter = 1;

    for (int row = 1; row < size; row++) {
        for (int col = 1; col < size; col++) {
            [cachedValues appendFormat:@"%d ", counter];
            counter += 1;
        }
        [cachedValues appendFormat:@"%d\n", counter];
        counter += 1;
    }

    for (int col = 1; col < size; col++) {
            [cachedValues appendFormat:@"%d ", counter];
            counter += 1;
    }
    [cachedValues appendFormat:@"%d", counter];
    
     NSLog(@"%@", cachedValues);
}
@end