//
//  Natural.m
//  Home task
//
//  Created by Fantom on 1/26/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Natural.h"

@implementation Natural

- (id)initWithNaturalLimit:(int)limit
{
    self = [super init];
    if (self) {
        NSMutableString* naturalNumbers = [NSMutableString new];
        
        for (int i = 1; i < limit; i++) {
            [naturalNumbers appendFormat:@"%d ", i];
        }
        [naturalNumbers appendFormat:@"%d", limit];
        
        NSLog(@"%@", naturalNumbers);
    }
    return self;
}

@end